package es.mrobles.adoptaunanimal;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.webkit.WebSettings;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.crash.FirebaseCrash;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import im.delight.android.webview.AdvancedWebView;

public class MainActivity extends Activity implements AdvancedWebView.Listener {

    private static final int MY_PERMISSIONS_REQUEST = 1;
    private AdvancedWebView mWebView;
    private FirebaseAnalytics mFirebaseAnalytics;
    private final String PROTOCOL = "https://";
    protected final String HOST = "www.guapet.es";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        mWebView = (AdvancedWebView) findViewById(R.id.webview);

        checkPermisos();

        mWebView.setListener(this, this);

        if (savedInstanceState == null) {
            mWebView.loadUrl(PROTOCOL + HOST);
        }

        List<String> hosts = Arrays.asList(
                HOST,
//                "www.evirom.com",
                "www.facebook.com",
                "www.twitter.com",
                "www.instagram.com"
        );

        mWebView.getSettings().setSupportMultipleWindows(true);
        mWebView.addPermittedHostnames(hosts);
        mWebView.setGeolocationEnabled(true);
        mWebView.setMixedContentAllowed(false);
        mWebView.getSettings().setPluginState(WebSettings.PluginState.ON);

        mWebView.setWebChromeClient(new MyWebClient(this));


    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mWebView.saveState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mWebView.restoreState(savedInstanceState);
    }

    @SuppressLint("NewApi")
    @Override
    protected void onResume() {
        super.onResume();
        mWebView.onResume();
    }

    @SuppressLint("NewApi")
    @Override
    protected void onPause() {
        mWebView.onPause();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        mWebView.onDestroy();
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        mWebView.onActivityResult(requestCode, resultCode, intent);
    }

    @Override
    public void onBackPressed() {
        if (!mWebView.onBackPressed()) {
            return;
        }
        super.onBackPressed();
    }

    @Override
    public void onPageStarted(String url, Bitmap favicon) {
//        Log.e("START", url);
    }

    @Override
    public void onPageFinished(String url) {
//        Log.e("END", url);
    }

    @Override
    public void onPageError(int errorCode, String description, String failingUrl) {
        FirebaseCrash.log("URL: " + failingUrl + " DESCRIPCION: " + description + " CODIGO: " + errorCode);
        mWebView.loadUrl("file:///android_asset/html/index.html");

        Toast toast = Toast.makeText(getBaseContext(),
                "Whopps, algo ha fallado.",
                Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.TOP | Gravity.CENTER, 0, 0);
        toast.show();
    }

    @Override
    public void onDownloadRequested(String url, String suggestedFilename, String mimeType, long contentLength, String contentDisposition, String userAgent) {
//        Log.e("DOWNLOAD", url);
    }

    @Override
    public void onExternalPageRequest(String url) {
        String dominio = Uri.parse(url).getHost();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            if ((url.startsWith("http:") || url.startsWith("https:")) && Objects.equals(dominio, HOST)) {
                mWebView.loadUrl(url);
            } else {
                Uri uri = Uri.parse(url);
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        }else{
            if ((url.startsWith("http:") || url.startsWith("https:")) && dominio.equals(HOST)) {
                mWebView.loadUrl(url);
            } else {
                Uri uri = Uri.parse(url);
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        }

    }

    private void checkPermisos() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

            } else {


                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST);

            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

        }
    }

}


